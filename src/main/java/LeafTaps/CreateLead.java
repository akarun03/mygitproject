package LeafTaps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class CreateLead extends SeleniumBase {
	
	@DataProvider()
	public String[][] mydata()
	{
		
	}
	
	
	

	@Test
	public void createLead() {

		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
//Username
		WebElement Username = locateElement("id", "username");
		clearAndType(Username, "DemoSalesManager");
//Password
		WebElement Password = locateElement("id", "password");
		clearAndType(Password, "crmsfa");
//Login
		WebElement Login = locateElement("class", "decorativeSubmit");
		click(Login);

//Click the CRM
		WebElement ClickCRM = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(ClickCRM);
//ClickLead
		WebElement ClickLead = locateElement("xpath", "//a[contains(text(),'Leads')]");
		click(ClickLead);
//CreateLead
		WebElement CreatLead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(CreatLead);
//Enter the Company Name
		WebElement ComName = locateElement("id", "createLeadForm_companyName");
		clearAndType(ComName, "BSS");
//Enter the firstName
		WebElement FirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(FirstName, "Arun");
		WebElement LastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(LastName, "A.K");
//Source
		WebElement DropDo = locateElement("id", "createLeadForm_dataSourceId");
		Select Sel = new Select(DropDo);
		Sel.selectByVisibleText("Employee");

		WebElement Markaet = locateElement("id", "createLeadForm_marketingCampaignId");
		Select Sel1 = new Select(Markaet);
		Sel1.selectByIndex(2);

		WebElement Email = locateElement("id", "createLeadForm_primaryEmail");
		clearAndType(Email, "akarun03@gmail.com");

		WebElement Country = locateElement("id", "createLeadForm_generalCountryGeoId");
		Select Sel2 = new Select(Country);
		Sel2.selectByVisibleText("India");

		WebElement CreLead = locateElement("class", "smallSubmit");
		click(CreLead);

		// System.out.println("you created has been sucessfully");

		// Verify the CreateLead

		WebElement FirstName1 = locateElement("id", "createLeadForm_companyName");
		WebElement lastName2 = locateElement("id", "createLeadForm_lastName");
		String Str = getElementText(FirstName1);
		String Str1 = getElementText(lastName2);

	}

}
