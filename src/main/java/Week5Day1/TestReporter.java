package Week5Day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class TestReporter {
	
	ExtentHtmlReporter html;//Creating the new template
	ExtentReports extent;//Attaching the file
	ExtentTest test;//
	@Test
	public void runreport() {
		
		html =new ExtentHtmlReporter("./Report/extentReport.html");
		extent=new ExtentReports();
		extent.attachReporter(html);
		test=extent.createTest("TC001_LoginAndLogout","Login into Leaftaps");
	    test.assignAuthor("Joanna");
	    test.assignCategory("smoke");
	    
	    try {
			test.pass("your entered Created Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		extent.flush();
		
		
	}
	
	

}
