package bl.framework.api;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import bi.framework.design.ProjectsMethods;

public class Demo extends ProjectsMethods {

	@Test
	public void login() {


		WebElement Cli = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(Cli);

		WebElement Mylead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(Mylead);

		WebElement CreLead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(CreLead);

		/*
		 * WebElement DropDown = driver.findElementById("createLeadForm_dataSourceId");
		 * Select Sel=new Select(DropDown); Sel.selectByVisibleText("ZZZ");
		 * Sel.selectByIndex(2) Sel.selectByValue("");
		 */

		// WebElement Mylea = locateElement("xpath", "//a[contains(text(),'My
		// Leads')]");
		// click(Mylea);

		// WebElement ClLIk = locateElement("xpath",
		// "//a[contains(text(),'eCommerce')]");
		// click(ClLIk);

		// WebElement ClickLink = locateElement("xpath", "//a[contains(text(),'Docs
		// Wiki')]");
		// click(ClickLink);

//Enter the Company name
		WebElement ComName = locateElement("id", "createLeadForm_companyName");
		clearAndType(ComName, "Apple");
//Enter First Name
		WebElement FirName = locateElement("id", "createLeadForm_firstName");
		clearAndType(FirName, "Joanna");
//Enter the last Name
		WebElement LasName = locateElement("id", "createLeadForm_lastName");
		clearAndType(LasName, "Vignesh");
//DropDown
		WebElement eleDropdown = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingValue(eleDropdown, "Employee");
		// WebElement eleSourceDD = locateElement("id","createLeadForm_dataSourceId");
		// selectDropDownUsingValue(eleSourceDD, "LEAD_PARTNER");
//Market
		WebElement Market = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(Market, "Automobile");
//firstname local
		WebElement FirLoc = locateElement("id", "createLeadForm_firstNameLocal");
		clearAndType(FirLoc, "PK");
//LastName Local
		WebElement LasLoc = locateElement("id", "createLeadForm_lastNameLocal");clearAndType(LasLoc, "Jashvi");
		
//Salutation//DOB//Title//Dept//Annual //Curreny country//Employee//industry//Ownership//SIC Code//TickerSYM
		
		WebElement Sal = locateElement("id", "createLeadForm_personalTitle");clearAndType(Sal, "Tamil Culture");
		WebElement DOB = locateElement("id", "createLeadForm_birthDate");clearAndType(DOB, "25/05/1991");
		WebElement Title = locateElement("id", "createLeadForm_generalProfTitle");clearAndType(Title, "Mr");
		WebElement Dept = locateElement("id", "createLeadForm_departmentName");clearAndType(Dept,"MCA");
		WebElement Annual = locateElement("id", "createLeadForm_annualRevenue");clearAndType(Annual, "100000");
		WebElement Drop = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(Drop, "BRR-Brazil");
		WebElement Indus = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(Indus, "Insurance");
		WebElement NoofEmpl = locateElement("id", "createLeadForm_numberEmployees");clearAndType(NoofEmpl, "600");
		WebElement Owner = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(Owner, "Corporation");
		WebElement SIC = locateElement("id", "createLeadForm_sicCode");clearAndType(SIC, "3999");
		WebElement TickerSym = locateElement("id", "createLeadForm_tickerSymbol");clearAndType(TickerSym, "2356");
		WebElement Descript = locateElement("id", "createLeadForm_description");clearAndType(Descript, "need to focus on automation");
		WebElement Descpt = locateElement("id", "createLeadForm_importantNote");clearAndType(Descpt, "High Impact");
		
		
		
//Country Code
		WebElement Code = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		clearAndType(Code, "91");
//Phone Number
		WebElement Phoe = locateElement("id", "createLeadForm_primaryPhoneNumber");
		clearAndType(Phoe, "7418556677");
		WebElement Email = locateElement("id", "createLeadForm_primaryEmail");clearAndType(Email, "akar@gamail.com");
		
//Primary Section
//to Name
		
		WebElement NameLi = locateElement("id", "createLeadForm_generalToName");
		clearAndType(NameLi, "Prabas");
		WebElement Address1 = locateElement("id", "createLeadForm_generalAddress1");clearAndType(Address1, "Sathya St.,");
		
		WebElement CityNam = locateElement("id", "createLeadForm_generalCity");clearAndType(CityNam, "Chennai");
		
		WebElement Tami = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		clearAndType(Tami, "TamilNadu");
		
		WebElement Count = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(Count, "India");
		
		WebElement Pincoe = locateElement("id", "createLeadForm_generalPostalCode");
		clearAndType(Pincoe, "6383683");
		
		
		WebElement Createlaed = locateElement("id", "ext-gen600");
		click(Createlaed);
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
