package bl.framework.api;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectsMethods;
import bl.framework.api.SeleniumBase;

public class TestCaseDupli extends ProjectsMethods {
    
	@Test
	public void DupiLead() throws InterruptedException {

		
		
		
		WebElement CRMSFALink = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(CRMSFALink);
		
		WebElement LeadCli = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(LeadCli);
		
		WebElement CreLead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(CreLead);
		
		WebElement ClikFi = locateElement("xpath", "//a[contains(text(),'Find Leads')]");
		click(ClikFi);
		
		WebElement Email = locateElement("xpath", "//span[contains(text(),'Email')]");
		click(Email);
		
		WebElement EmailN = locateElement("xpath", "//input[@name='emailAddress']");
		clearAndType(EmailN, "akarun03@gmail.com");
		
		//Click on find leads button -
		
		WebElement eleFindLeadsBtn = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsBtn);
		
		//select first matching record
		//identify table - 
		
		WebElement eleTableName = locateElement("xpath", "(//table[@class='x-grid3-row-table'])[1]");
		  List<WebElement> rows = eleTableName.findElements(By.tagName("tr"));
		 WebElement firstrow = rows.get(0);
	 List<WebElement> columns = firstrow.findElements(By.tagName("td"));
		 String firstMatchingLeadid=columns.get(0).getText();
		 
		 WebElement eleFirstMatchingLeadId = locateElement("linktext",firstMatchingLeadid );
		 click(eleFirstMatchingLeadId);
		 
		 //Click on Duplicate lead - 
		 WebElement eleDuplicateLeadLink = locateElement("xpath", "//a[text()='Duplicate Lead']");
		 click(eleDuplicateLeadLink);
		 
		 Thread.sleep(2000);
		 
		 //Verify Url
		 boolean verifyTitle = verifyTitle("Duplicate Lead");
		 
		 if(verifyTitle)
			 System.out.println("given value  Duplicate Lead Title is matching");
		 else
			 System.out.println("Given value is not matching with the value");
		 
		 //Click on create lead  - 
		 
		 WebElement eleCreateLeadBtn = locateElement("xpath", "//input[@value='Create Lead']");
		 click(eleCreateLeadBtn);
		 
		 //Verify the duplicate lead name same as captured name
		 
		//verifying lead creation
			//viewLead_firstName_sp
			WebElement eleFirstNameLabel = locateElement("id", "viewLead_firstName_sp");
			WebElement eleLastNameLabel = locateElement("id", "viewLead_lastName_sp");
			String txtFstName = getElementText(eleFirstNameLabel);
			String txtLstName = getElementText(eleLastNameLabel);
			
			if((txtFstName.equals("Matthew")) && (txtLstName.equals("Booker")))
			{
				System.out.println(" Duplicate Lead is created successfully");
			}
			
			else
				System.out.println("Duplicate is not created");	
	    
	}

}
